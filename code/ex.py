# -*- coding: utf-8 -*-

# Формулировка задачи:
# 1. Составить программу, выполняющую лемматизацию выбранного объемного текста на русском языке,
# а также подсчет частоты и рангов различных словоформ и лемм.
# 2. Построить графики зависимости относительной частоты от ранга словоформы и леммы, соответственно.
# 3. Проверить выполнение закона Ципфа-Мальдеброта

from re import findall as fa, split as sp
from pymorphy2 import MorphAnalyzer as ma
from pathlib import Path
from tqdm import tqdm
import xml.etree.ElementTree as ET


class Word(object):

    name  = 'default'
    count = -1
    rank  = -1

    def __init__(self, params):
        self.name = params[0]
        self.count = params[1]
        self.rank = params[2]

    def get_list(self):
        """
        :return:all self fields as list
        """
        return [self.name, str(self.count), str(self.rank)]

    def __repr__(self):
        return repr((self.name, self.count, self.rank))


def gen(path, leng='ru'):
    """
    Processing file: finding all worforms ans lemmas, counts and calculate ranks
    + saving data in .csv
    :param path: path
    """
    try:
        for i in tqdm(range(1), desc='Parsing'):
            root = ET.parse(path)
    except Exception:
        print("Can't find such file")
        return
    # получаем список всех размеченых предложений
    for i in tqdm(range(1), desc='Getting list of sentences'):
        sentences = root.findall('.//source')

    words = []
    n_words = []
    for sentence in tqdm(sentences, desc="Reading sentences"):
        if leng == 'en':
            words += fa(r"(?:\s|\b)([a-zA-Z]+)(?:\s|\b)", sentence.text)
        else:
            words += fa(r"(?:\s|\b)([а-яА-ЯёЁ]+)(?:\s|\b)", sentence.text)

    # создаем словарь для учета уникальных нормальных форм
    wordforms = {}
    lemmas = {}

    # настройка морфологического анализатора на работу с русским языком
    morph = ma(lang='ru')

    # проходимся по всем словам
    for word in tqdm(words, desc="Lematization"):

        normal_form = morph.parse(word)[0].normal_form
        n_words.append(normal_form)

        if word in wordforms:
            # увеличиваем счетчик в словаре если нормальная форма уже была встречена ранее
            wordforms[word] += 1
        else:
            # добавляем новый ключ в словарь
            wordforms[word] = 1

        if normal_form in lemmas:
            # увеличиваем счетчик в словаре если нормальная форма уже была встречена ранее
            lemmas[normal_form] = lemmas.get(normal_form) + 1
        else:
            # добавляем новый ключ в словарь
            lemmas[normal_form] = 1

    wordforms_rank = {}
    lemmas_rank = {}

    # вычисляем ранги для лемм и словоформ
    i = 1
    for item in sorted(set(wordforms.values()), reverse=True):
        wordforms_rank[item] = i
        i += 1

    i = 1
    for item in sorted(set(lemmas.values()), reverse=True):
        lemmas_rank[item] = i
        i += 1

    wordforms_table = []
    lemmas_table = []

    # формируем список из словоформ
    for word in wordforms:
        count = wordforms[word]
        rank = wordforms_rank[count]
        wordforms_table.append(Word([word, count, rank]))
    # формируем списков из лемм
    for lemma in lemmas:
        count = lemmas[lemma]
        rank = lemmas_rank[count]
        lemmas_table.append(Word([lemma, count, rank]))

    # сортируем оба списка по параметру ранг
    wordforms_table = sorted(wordforms_table, key=lambda word: word.rank)
    lemmas_table = sorted(lemmas_table, key=lambda word: word.rank)

    # создаем выходные файлы
    gen_output(path, 'lemma', lemmas_table)
    gen_output(path, 'wordform', wordforms_table)

    # печать 50-ти самых популярных
    print_popular(wordforms_table, 'wordform')
    print_popular(lemmas_table, 'lemma')


def print_popular(source, tag):
    print("Самые популярные " + tag)
    for word in source[:50]:
        print(word)


def gen_output(path, tag, source):
    """
    Generate output file in csv format in folder ../tables
    :param path: path
    :param tag: 'lemma' or 'wordform'
    :param source: list of Words
    """
    path = Path(path).stem
    try:
        file = open("../tables/" + path + '_' + tag + '.csv', 'w', encoding='utf-8')
        file.write('word, count, rank \n')
        for word in tqdm(source, desc='Creating output file'):
            file.write(",".join(word.get_list()) + '\n')
    except Exception:
        print("Can't create output file")
        return


def gen_input(path, tag):
    """
    reading csv files
    :param path: path
    :param tag: 'lemma' or 'wordform'
    :return: list of words
    """
    path = Path(path).stem
    try:
        file = open("../tables/" + path + '_' + tag + '.csv', encoding='utf-8')
        file.readline()
        words = []
        for line in file.readlines():
            words.append(Word(line.replace('\n', '').split(',')))
        return words
    except Exception:
        print("Can't find such file")
        return


if __name__ == '__main__':
    print('Введите имя файла: ')
    filename = input()

    # filename = '../data/annot.opcorpora.xml' // не рекомендуется использовать, тк займет много времени обработка.
    # filename = '../data/vim.xml'
    # filename = '../data/4063.xml'

    # Используется для анализа собственного текста
    gen(filename)

    # Используется для работы уже с обработанным текстом
    # words = gen_input(filename, 'lemmas')
    # print_popular(words, 'lemma')
